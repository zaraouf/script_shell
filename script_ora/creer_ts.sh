#!/bin/bash

if [ $# -ne 3 ]; then
	echo "nombre de parametre incorrecte: CONNEXION_FILE NAME_TS SIZE"
	exit 1
fi

if [ ! -f "$1" ]; then
    echo "Le fichier de connexion '$1' n'existe pas."
    exit 1
fi

source $1
NAME_TS=$2
SIZE=$3

sqlplus $USER_ORA/$PASSWORD_ORA@$IP_SRV:$PORT_ORA/$BDD << FINSQL

create TABLESPACE $NAME_TS DATAFILE '/home/oracle/TS_G8/${NAME_TS}.dbf' size ${SIZE}M;

FINSQL

