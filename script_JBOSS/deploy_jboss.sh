#!/bin/bash

#if [ $# -ne 3 ]; then
#	echo "Nombre d'arguments incorrect CONNEXION_FILE APPLICATION SRV_GROUP"
#fi

echo "Entre le chemin vers le fichier de connexion :"
read CONNEXION

source $CONNEXION

echo "Entre le chemin vers l'application"
read APP

echo "Entre le nom du server group"
read SRV_GROUP
i=1
APP_NAME=$(basename "$APP")
/opt/wildfly/bin/jboss-cli.sh --connect --controller=${IP_JBOSS}:${PORT_JBOSS} --user=${USER_JBOSS} --password=${PASSWORD_JBOSS} -c "deploy ${APP} --server-groups=${SRV_GROUP}"
while [ $? -ne 0 ]; do
	((i++))
	/opt/wildfly/bin/jboss-cli.sh --connect --controller=${IP_JBOSS}:${PORT_JBOSS} --user=${USER_JBOSS} --password=${PASSWORD_JBOSS} -c "deploy ${APP} --server-groups=${SRV_GROUP} --name=${i}_${APP_NAME}"
done

