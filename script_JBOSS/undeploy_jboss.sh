#!/bin/bash
if [ $# -ne 3 ]; then
        echo "Nombre d'arguments incorrect CONNEXION_FILE APPLICATION SRV_GROUP"
        exit 1
fi

echo "Entre le  chemin du fichier de connexion :"
read CONNEXION

source $CONNEXION

echo "Entre le  Nom du Serveur groupe :"
read SRV_GROUP

echo "Entre le nom de l'application :"
read APP_NAME

/opt/wildfly/bin/jboss-cli.sh --connect --controller=${IP_JBOSS}:${PORT_JBOSS} --user=${USER_JBOSS} --password=${PASSWORD_JBOSS} -c "undeploy --server-groups=${SRV_GROUP}  --name=${APP_NAME}"

