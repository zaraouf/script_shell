#!/bin/bash

echo "Entre le fichier de connexion :"
read CONNEXION

source $CONNEXION

echo "Entre le nom du host :"
read HOST

echo "Entre le nom du serveur :"
read SERVER

echo "Entre l'action D/A/R"
read ACTION


if [ $ACTION = D ];then

	/opt/wildfly/bin/jboss-cli.sh --connect --controller=${IP_JBOSS}:${PORT_JBOSS} --user=${USER_JBOSS} --password=${PASSWORD_JBOSS} -c "/host=${HOST}/server-config=${SERVER}:start"
elif [ $ACTION = A ];then
	/opt/wildfly/bin/jboss-cli.sh --connect --controller=${IP_JBOSS}:${PORT_JBOSS} --user=${USER_JBOSS} --password=${PASSWORD_JBOSS} -c "/host=${HOST}/server-config=${SERVER}:stop"

elif [ $ACTION = R ];then
	/opt/wildfly/bin/jboss-cli.sh --connect --controller=${IP_JBOSS}:${PORT_JBOSS} --user=${USER_JBOSS} --password=${PASSWORD_JBOSS} -c "/host=${HOST}/server-config=${SERVER}:restart"

else
	echo "ERROR: action non reconnu"
	exit 2
fi
